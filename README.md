# Battleship generator

Battleship generator written in Vue.js version 3 with bare CSS.

## Set up
All the configuration is inside `js/config.js` file.
Consult that file and change whatever is necessary.

## Printing & use
After printing the games, there will always be one page with pre-arranged boards, followed by a page with empty boards.
The idea is to do a double-sided prints. That way, when all is printed out, one side of the paper will contain
pre-arranged boards, and the other side will contain empty boards.
After printing, you can cut boards on predefined cutlines, and use one side as your battleship fleet (to track hits by your opponent),
and the other side to track hits and misses you did to your opponent.

Best of luck!
