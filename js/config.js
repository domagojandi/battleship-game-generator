/**
 * PAGES configuration determines the number of pages of battleship boards to generate.
 * By default, every page will contain 12 games with pre-arranged battleships.
 * After every page goes one page with blank battleships, which helps the player to track opponent's battleships.
 * So, if for example PAGES variable is set to 5, the app will generate a total of 10 pages 
 * (5 with pre-arrangend ships, and 5 with no ships),
 * which is 60 (12x5) pre-arranged boards. That would be enough for 30 games.
 */
const PAGES = 5;