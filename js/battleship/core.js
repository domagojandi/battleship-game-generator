/**
 * Generates empty 8x8 battleship board
 * @param {*} fieldValue Defines what value to put to every board field. 
 *  Can be of any JSON-seralizable type
 */
function generateEmptyBattleshipBoard(fieldValue=0) {
  const board = [];
  for (let _i=0; _i < 8; _i++) {
    const boardRow = [];
    for (let _j=0; _j < 8; _j++) {
      // duplicating the value to prevent referencing the same object (when updating it later on)
      const uniqueFieldValue = JSON.parse(JSON.stringify(fieldValue));
      boardRow.push(uniqueFieldValue);
    }
    board.push(boardRow);
  }
  return board;
}