/**
 * Generates a battleship with ships on positions
 */
class BattleshipGenerator {
  constructor() {
    this._board = generateEmptyBattleshipBoard(0);
    this._shipPositions = [];
  }

  static _getRandomPointOnBoard() {
    const posx = getRandomNumber(0, 7);
    const posy = getRandomNumber(0, 7);
    return new Point(posx, posy);
  }

  /**
   * Logs generated board to console.
   * If ships are not yet generated on board, calling this method will generate them.
   */
  logBoard() {
    this.generate();
    for (const row of this._board) {
      console.log(JSON.stringify(row).replace(/,/g, ' '));
    }
    console.log('Positions:');
    for (const pos of this._shipPositions) {
      console.log(`Starting: (${pos.startingCoordinates.x+1},${pos.startingCoordinates.y+1}), length: ${pos.length}, orientation: ${ShipOrientation.getOrientationName(pos.orientation)}`);
    }
  }

  _isCoordinateTakenOnBoard(coordinate) {
    if (coordinate.x > 7 || coordinate.y > 7 || coordinate.x < 0 || coordinate.y < 0) {
      return true;
    } else {
      return this._board[coordinate.y][coordinate.x] > 0;
    }
  }

  _markCoordinateTakenOnBoard(coordinate, value) {
    if (coordinate.x > 7 || coordinate.y > 7 || coordinate.x < 0 || coordinate.y < 0) {
      throw new Error(`Ship coordinate out of range: Point(${coordinate.x},${coordinate.y})`);
    } else {
      return this._board[coordinate.y][coordinate.x] = value;
    }
  }

  /**
   * Tries to position ship on board. If succeeded, returns True,
   * and positions ship on board
   * Otherwise returns false
   */
  _positionShipOnBoard(shipPosition) {
    const shipCoordinates = [];
    if (this._isCoordinateTakenOnBoard(shipPosition.startingCoordinates)) {
      return false;
    } else {
      shipCoordinates.push(shipPosition.startingCoordinates.clone());
    }
    for (let i=1; i<shipPosition.length; i++) {
      const nextShipCoordinate = ShipOrientation.getNextCoordinateForPosition(shipCoordinates[i-1], shipPosition.orientation);
      if (this._isCoordinateTakenOnBoard(nextShipCoordinate)) {
        return false;
      } else {
        shipCoordinates.push(nextShipCoordinate);
      }
    }

    // if we didn't return already (all coordinates were good)
    // put ship on board and return true
    for (const coordinate of shipCoordinates) {
      this._markCoordinateTakenOnBoard(coordinate, shipPosition.length);
    }

    return true;
  }

  _generateNewShipOnBoard(shipSize) {
    const shipPosition = {startingCoordinates: null, length: shipSize, orientation: null};

    // try to put ship somewhere on board
    let shipGenerated = false;
    do {
      // first generate random starting coordinates
      shipPosition.startingCoordinates = BattleshipGenerator._getRandomPointOnBoard();
      const orientationsTried = new Set();
      // then, on generated coordinates, try to put ship in any orientation possible
      while (orientationsTried.size < 4 && !shipGenerated) {
        const randomOrientation = ShipOrientation.getRandomOrientation();
        if (orientationsTried.has(randomOrientation)) {
          continue;
        }
        orientationsTried.add(randomOrientation);
        shipPosition.orientation = randomOrientation;
        if (this._positionShipOnBoard(shipPosition)) {
          shipGenerated = true;
        }
      }
    } while (!shipGenerated);
    
    this._shipPositions.push(shipPosition);
  }

  generate() {
    if (this._shipPositions.length === 0) {
      const shipLengths = [2,3,3,4,5];
      // randomize ship lengths
      while (shipLengths.length > 0) {
        const randomShipLengthIndex = getRandomNumber(0, shipLengths.length - 1);
        const randomShipLength = shipLengths.splice(randomShipLengthIndex, 1)[0];
        this._generateNewShipOnBoard(randomShipLength);
      }
    }
    return this;
  }

  getShipPositions() {
    this.generate();
    return this._shipPositions;
  }
}
