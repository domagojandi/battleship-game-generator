
/**
 * Generates Vue-adapted battleship board (to be used on UI)
 */
class BattleshipVueAdapter {
  /**
   * Converts BattleshipGenerator output to empty array.
   *  If BattleshipGenerator instance is not provided, this will output empty battleship field
   * @param {BattleshipGenerator} battleship 
   */
  constructor(battleship = null) {
    this._battleship = battleship;
    this._vueAdaptedShipPositionsGenerated = battleship === null;
    this._vueAdaptedShipPositions = generateEmptyBattleshipBoard({taken: false});
  }

  _generateVueAdaptedShipPositions() {
    for (const shipPosition of this._battleship.getShipPositions()) {
      let shipPieceNumber = 0;
      let lastShipPiecePosition = null;
      do {
        shipPieceNumber++;
        if (!lastShipPiecePosition) {
          lastShipPiecePosition = shipPosition.startingCoordinates;
        } else {
          lastShipPiecePosition = ShipOrientation.getNextCoordinateForPosition(
            lastShipPiecePosition, shipPosition.orientation);
        }
        const shipPiece = this._vueAdaptedShipPositions[lastShipPiecePosition.y][lastShipPiecePosition.x];
        shipPiece.taken = true;
        shipPiece.orientation = shipPosition.orientation;
        shipPiece.isFirstPiece = shipPieceNumber === 1;
        shipPiece.isLastPiece = shipPieceNumber === shipPosition.length;
      } while (shipPieceNumber < shipPosition.length);
    }
  }

  generate() {
    if (!this._vueAdaptedShipPositionsGenerated) {
      this._generateVueAdaptedShipPositions();
      this._vueAdaptedShipPositionsGenerated = true;
    }
    return this;
  }

  getShipPositions() {
    this.generate();
    return this._vueAdaptedShipPositions;
  }
}
