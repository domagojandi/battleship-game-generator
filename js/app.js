/**
 * Represents container of the battleship templates.
 */
Vue.createApp({
  data() {
    const boards = [];
    const NUM_BOARDS = 12;  // 12 boards best fit to A4 paper
    for (let _pagenum=1; _pagenum<=PAGES; _pagenum++) {
      const boardsWithShips = [];
      const emptyBoards = [];
      // generate boards with boats on them
      for (let _i=0; _i<NUM_BOARDS; _i++) {
        const board = (new BattleshipVueAdapter(new BattleshipGenerator())).getShipPositions();
        boardsWithShips.push(board);  
      }
      // generate empty boards
      for (let _i=0; _i<NUM_BOARDS; _i++) {
        const emptyBoard = (new BattleshipVueAdapter()).getShipPositions();
        emptyBoards.push(emptyBoard);
      }
      boards.push(boardsWithShips);
      boards.push(emptyBoards);
    }

    return {
      boards,
    };
  },
  methods: {
    getRowCode(rowNumber) {
      return String.fromCharCode(65 + rowNumber);
    },
    boardPieceStyles(board, rowIndex, colIndex) {
      const boardPiece = board[rowIndex][colIndex];
      return {
        'ship-piece': boardPiece.taken,
        'ship-left': ShipOrientation.getOrientationName(boardPiece.orientation) === 'LEFT',
        'ship-right': ShipOrientation.getOrientationName(boardPiece.orientation) === 'RIGHT',
        'ship-up': ShipOrientation.getOrientationName(boardPiece.orientation) === 'UP',
        'ship-down': ShipOrientation.getOrientationName(boardPiece.orientation) === 'DOWN',
        'ship-first-piece': boardPiece.isFirstPiece,
        'ship-last-piece': boardPiece.isLastPiece,
      }
    }
  }
}).mount('#battleship-boards');
