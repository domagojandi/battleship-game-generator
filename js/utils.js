
/**
 * Generates integer between lower and upper bound (both inclusive)
 * @param {Number} lowerBound Number which indicates lower bound for the generated number (included)
 * @param {Number} upperBound Number which indicates upper bound for the generated number (included)
 */
function getRandomNumber(lowerBound, upperBound) {
  return Math.floor(Math.random() * (upperBound - lowerBound + 1)) + lowerBound;
}


class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y; 
  }
  clone() {
    return new Point(this.x, this.y);
  }
}


const ShipOrientation = Object.freeze({
  UP: 0,
  DOWN: 1,
  LEFT: 2,
  RIGHT: 3,
  getOrientationName(orientation) {
    switch (orientation) {
      case this.UP: return 'UP';
      case this.DOWN: return 'DOWN';
      case this.LEFT: return 'LEFT';
      case this.RIGHT: return 'RIGHT';
    }
  },
  /**
   * Return orientation as UP, DOWN, LEFT or RIGHT
   */
  getRandomOrientation() {
    return getRandomNumber(0, 3);
  },
  /**
   * Given the coordinate point, returns next coordinate based on orientation
   * @param {Point} coordinate Initial coordinate
   * @param {*} orientation Orientation - UP, DOWN, LEFT or RIGHT
   * @returns {Point} New coordinate after moving ``coordinate`` to a specified ``orientation`` 
   */
  getNextCoordinateForPosition(coordinate, orientation) {
    const newCoordinate = coordinate.clone();
    switch (orientation) {
      case this.UP: 
        newCoordinate.y--; break;
      case this.DOWN: 
        newCoordinate.y++; break;
      case this.LEFT:
        newCoordinate.x--; break;
        case this.RIGHT:
          newCoordinate.x++; break;
    }
    return newCoordinate;
  },
});
